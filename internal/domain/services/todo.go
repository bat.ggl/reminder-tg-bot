package services

import (
	"context"
	"errors"
	v1 "gitlab.com/bat.ggl/reminder-bot/internal/interfaces/grpc/api/v1"
	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"time"
)

type TodoService struct {
	logger     *zap.SugaredLogger
	todoClient v1.TodoServiceClient
}

func NewTodoService(logger *zap.SugaredLogger, todoClient v1.TodoServiceClient) *TodoService {
	return &TodoService{
		logger:     logger,
		todoClient: todoClient,
	}
}

func (t *TodoService) CreateTodo(userID int32, projectID int, description string) error {
	ctx, _ := context.WithTimeout(context.Background(), time.Millisecond * 60)


	tags := make([]int32, 0)
	tags = append(tags, 1)

	data := &v1.CreateRequest{
		Todo: &v1.Todo{
			UserID:      userID,
			ProjectID:   int32(projectID),
			Description: description,
			Name: "asdasdasd",
			Tags: tags,
		},
	}

	response, err := t.todoClient.CreateTodo(ctx, data)
	if err != nil {
		if e, ok := status.FromError(err); ok {
			switch e.Code() {
			case codes.InvalidArgument:
				t.logger.Error(e.Message())
				return errors.New("Invalid argument")
			case codes.Internal:
				t.logger.Error(e.Message())
				return errors.New("Internal error")
			}
		}

		t.logger.Error(err.Error())
		return errors.New("Internal error")
	}

	if response.Status != true {
		return errors.New("failed to create new todo")
	}

	return nil
}

func (t *TodoService) GetTodos(userId int, projectID int) ([]*v1.Todo , error) {
	ids := make([]int32,0)
	ids = append(ids, int32(projectID))

	ctx, _ := context.WithTimeout(context.Background(), time.Millisecond * 200)
	request := &v1.GetTodosRequest{
		UserID:    int32(userId),
		StartID:   0,
		ProjectID: ids,
		Status:    0,
		From:      0,
		To:        0,
		Limit:     0,
	}

	response, err := t.todoClient.GetTodos(ctx, request)
	if err != nil {
		if e, ok := status.FromError(err); ok {
			switch e.Code() {
			case codes.InvalidArgument:
				t.logger.Error(e.Message())
				return nil, errors.New("Invalid argument")
			case codes.Internal:
				t.logger.Error(e.Message())
				return nil, errors.New("Internal error")
			}
		}

		t.logger.Error(err.Error())
		return nil, errors.New("Internal error")
	}

	return response.Todos, nil
}
