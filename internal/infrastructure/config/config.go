package config

import "github.com/spf13/viper"

type Config struct {
	IsProd         bool   `mapstructure:"is_prod"`
	ServerAddr     int    `mapstructure:"server_server"`
	ServerGRPCAddr int    `mapstructure:"grpc_server"`
	Token          string `mapstructure:"token"`
}

func NewConfig() (*Config, error) {
	config := &Config{}
	viper.AutomaticEnv()
	viper.SetDefault("is_prod", true)
	viper.SetDefault("http_server", 8080)
	viper.SetDefault("grpc_server", 5051)
	viper.SetDefault("token", "1250650695:AAEngrLS9wJls6Qybdo1auf1z6vwlGlEkF4")
	if err := viper.Unmarshal(config); err != nil {
		return nil, err
	}

	return config, nil
}
