package todo

import (
	v1 "gitlab.com/bat.ggl/reminder-bot/internal/interfaces/grpc/api/v1"
	"google.golang.org/grpc"
)

const todoapiurl = "localhost:5052"

type TodoClient struct {
	Client v1.TodoServiceClient
}

func CreateTodoClient() (*TodoClient, error) {
	todoClient := &TodoClient{}
	conn, err := grpc.Dial(todoapiurl, grpc.WithInsecure())
	if err != nil {
		panic(err)
	}

	todoClient.Client = v1.NewTodoServiceClient(conn)

	return todoClient, nil
}
