package application

import (
	"gitlab.com/bat.ggl/reminder-bot/internal/domain/services"
)

type ServiceContainer struct {
	Todo  *services.TodoService
}

func NewContainerService(
	todo  *services.TodoService,
) *ServiceContainer {
	return &ServiceContainer{
		Todo:  todo,
	}
}
