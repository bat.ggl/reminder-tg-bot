package application

import (
	"gitlab.com/bat.ggl/reminder-bot/internal/application"
	"gitlab.com/bat.ggl/reminder-bot/internal/domain/services"
	"gitlab.com/bat.ggl/reminder-bot/internal/infrastructure/clients/todo"
	"gitlab.com/bat.ggl/reminder-bot/internal/infrastructure/config"
	"gitlab.com/bat.ggl/reminder-bot/internal/interfaces/telegram"
	"go.uber.org/zap"
)

type Application struct {
	config config.Config
	logger *zap.SugaredLogger
}

func NewApplication(config config.Config, logger *zap.SugaredLogger) *Application {
	return &Application{
		config: config,
		logger: logger,
	}
}

func (a *Application) Run() {

	client, err := todo.CreateTodoClient()
	if err != nil {
		a.logger.Error(err)
	}

	tg, err := telegram.CreateTelegramClient(a.logger, a.config.Token)
	if err != nil {
		a.logger.Fatal(err)
	}

	todoService := services.NewTodoService(a.logger, client.Client)

	container := application.NewContainerService(todoService)

	tg.InitHandlers(container)
	tg.Run()
}

func (a *Application) Shutdown() {
}
