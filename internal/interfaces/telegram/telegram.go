package telegram

import (
	"fmt"
	"github.com/go-errors/errors"
	"gitlab.com/bat.ggl/reminder-bot/internal/application"
	"go.uber.org/zap"
	tb "gopkg.in/tucnak/telebot.v2"
	"time"
)

type TelegramClient struct {
	logger   *zap.SugaredLogger
	bot      *tb.Bot
	shutdown <-chan bool
}

func CreateTelegramClient(
	logger *zap.SugaredLogger,
	token string,
) (*TelegramClient, error) {
	b, err := tb.NewBot(tb.Settings{
		Token:  token,
		Poller: &tb.LongPoller{Timeout: 10 * time.Second},
	})
	if err != nil {
		return nil, errors.Errorf("failed to create bot client %w", err)
	}

	return &TelegramClient{
		logger:   logger,
		bot:      b,
	}, nil
}

func (t *TelegramClient) InitHandlers(container *application.ServiceContainer) {
	t.bot.Handle("/inbox", func(m *tb.Message){
		// projectID, err := strconv.Atoi(m.Text)
		// if err != nil {
		// 	t.logger.Error("Handle error: %s", err.Error())
		// 	t.bot.Send(m.Sender, fmt.Sprintf("ProjectID not be a string"))
		// }

		response, err := container.Todo.GetTodos(m.Sender.ID, 1)
		if err != nil {
			t.logger.Error("Handle error: %s", err.Error())
			t.bot.Send(m.Sender, fmt.Sprintf("Internal Error"))
			return
		}

		t.logger.Infof("user: %v", m.Sender.ID)
		t.bot.Send(m.Sender, fmt.Sprintf("resp: %s", response))
 	})

	t.bot.Handle(tb.OnText, func(m *tb.Message) {
		err := container.Todo.CreateTodo(int32(m.Sender.ID), 1, m.Text)
		if err != nil {
			t.logger.Info("Error: %s", err.Error())
			t.bot.Send(m.Sender, "Can't create todo =(")
			return
		}

		t.bot.Send(m.Sender, "ok to created")
	})


	// t.bot.Handle("/list", func(m *tb.Message) {
	// 	t.bot.Send(m.Sender, fmt.Sprintf("asdasdasd"))
	// })
	//
	// t.bot.Handle("/register", func(m *tb.Message) {
		// t.register.Register()
		// t.bot.Send(m.Sender, fmt.Sprintf("Hi %s, you are registred. Whait are minute", m.Sender.Username))
	// })
	//
	// t.bot.Handle("/inbox", func(m *tb.Message){
	// 	t.logger.Info("%s", m.Sender)
	// 	t.bot.Send(m.Sender, m.Text+"INBOX")
	// })
	//

}

func (t *TelegramClient) Run() {
	t.bot.Start()
}

func (t *TelegramClient) Stop() {
	t.bot.Stop()
}
