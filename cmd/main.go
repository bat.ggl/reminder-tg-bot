package main

import (
	application2 "gitlab.com/bat.ggl/reminder-bot/internal/application/application"
	"gitlab.com/bat.ggl/reminder-bot/internal/infrastructure/config"
	"go.uber.org/zap"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	stopping := make(chan os.Signal)
	log, err := zap.NewProduction()
	if err != nil {
		panic(err)
	}
	sugar := log.Sugar()

	config, err := config.NewConfig()
	if err != nil {
		sugar.Fatal(err)
	}

	app := application2.NewApplication(*config, sugar)
	go shutdownMonitor(stopping, app)
	app.Run()
}

func shutdownMonitor(stopping chan os.Signal, app *application2.Application) {
	signal.Notify(stopping, os.Interrupt, syscall.SIGTERM)
	for {
		for range stopping {
			app.Shutdown()
		}
	}
}
