module gitlab.com/bat.ggl/reminder-bot

go 1.14

require (
	github.com/go-chi/chi v4.1.2+incompatible // indirect
	github.com/go-errors/errors v1.1.1
	github.com/go-pg/pg v8.0.7+incompatible // indirect
	github.com/go-playground/validator/v10 v10.3.0 // indirect
	github.com/golang/protobuf v1.4.2
	github.com/grpc-ecosystem/go-grpc-prometheus v1.2.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/prometheus/client_golang v1.7.1 // indirect
	github.com/spf13/viper v1.7.1
	go.uber.org/zap v1.16.0
	google.golang.org/grpc v1.33.0
	google.golang.org/protobuf v1.25.0
	gopkg.in/tucnak/telebot.v2 v2.3.4
	mellium.im/sasl v0.2.1 // indirect
)
